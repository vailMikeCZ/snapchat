//
//  SelectUserViewController.swift
//  Snapchat
//
//  Created by michael krasilinec on 10/27/16.
//  Copyright © 2016 michael krasilinec. All rights reserved.
//

import UIKit
import Firebase

class SelectUserViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    //section 9 L 108
    var users : [User] = []
    
    var imageURL = ""
    //don't want to use description here as that's a keyword
    var descrip = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
       //FIRDatabase.database().reference().child("users").observe(FIRDataEventType.childAdded) { (snapshot) in
        FIRDatabase.database().reference().child("users").observe(FIRDataEventType.childAdded, with: { (snapshot) in
            print("mk snapshot: \(snapshot)")
            
            
            let user = User()
            //user.email = snapshot.value!["email"] as! String
            user.email = (snapshot.value as! NSDictionary) ["email"] as! String
            
            user.uid = snapshot.key
            
            //self since inside a completion block
            self.users.append(user)
            self.tableView.reloadData()
        })

        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let user = users[indexPath.row]
        cell.textLabel?.text = user.email
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = users[indexPath.row]
        
        let snap = ["from":user.email, "description":descrip,"imageURL":imageURL]
        
        FIRDatabase.database().reference().child("users").child(user.uid).child("snaps").childByAutoId().setValue(snap)
    }
}
